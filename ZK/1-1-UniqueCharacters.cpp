//  Implement an algorithm to determine if a string has all unique characters. What if you cannot use additional data structures?

// Time: O(n)
// Space: O(n)
bool hasAllUniqueChar(string input){
    unordered_set<char> charset;
    for (int i = 0; i < input.size(); i++){
        char c = input[i];
        if (charset.find(c) == charset.end()){
            charset.insert(c);
        }
        else{
            return false;
        }
    }

    return true;
}

// Time: O(nlogn)
// Space: O(1)
bool hasAllUniqueChar(string input){
    sort(input.begin(), input.end());
    if (input.size() > 1){
        for (int i = 1; i < input.size(); i++){
            char previousChar = input[i-1];
            if (input[i] == previousChar){
                return false;
            }
        }
    }
    
    return true;
}


int main(void){
    string testInput[] ={ "abcdefghijk", "abcdb" };
    int size = sizeof(testInput)/sizeof(testInput[0]);

    for (int i = 0; i < size; i++){
        bool result = hasAllUniqueChar(testInput[i]);
        cout << testInput[i] << ": " << result << "\n";
    }
    
    return 0;
}