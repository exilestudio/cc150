//Given a linked list, determine if it has a cycle in it.

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
class Solution {
public:
    bool hasCycle(ListNode *head) {
        if (NULL == head) return false;
        
        ListNode *low=head, *fast=head->next;
        
        while (NULL!=fast && NULL!=fast->next) {
            low = low->next;
            fast = fast->next->next;
            
            if (low == fast) return true;
        }
        
        return false;
    }
};