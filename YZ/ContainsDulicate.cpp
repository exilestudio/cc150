// Given an array of integers, find if the array contains any duplicates. Your function should return true if any value appears at least twice in the array, and it should return false if every element is distinct.

class Solution {
public:
    bool containsDuplicate(vector<int>& nums) {

    	//Better solution
        sort(nums.begin(), nums.end());
        for(int ind = 1; ind < nums.size(); ind++) {
            if(nums[ind] == nums[ind - 1]) {
                return true;
            }
        }
        return false;

/*
        int size = nums.size();
        
        if (size<1) return false;
        
        for (int i=0; i<size; i++){
            for (int j=i+1; j<size; j++)
                if (nums[i] == nums[j]) return true;
        }
        
        return false;*/ // Run into "Time Limit Exceeded"
    }
}; 