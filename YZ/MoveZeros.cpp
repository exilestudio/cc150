class Solution {
public:
    void moveZeroes(vector<int>& nums) {
        int size = nums.size();
        if (size < 1)
        	return;

        //vector<int> numsCopy(size); //If possible not use declare?
        int count = 0;

        for (int i=0; i<size; i++) {
        	if (nums[i] != 0) 
        		nums[count++] = nums[i];
        }

        while (count < size)
            nums[count++] = 0;
            
        //nums = numsCopy;
    }
};