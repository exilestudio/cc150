cclass Solution {
public:
    void rotate(vector<int>& nums, int k) {
        int size = nums.size();
        if (k<=0 || size<=1) return;
        
        vector<int> numsCopy(size);
        for (int i=0; i<size; i++){
            numsCopy[i] = nums[i];
        }
        
        for (int i=0; i<size; i++){
            nums[(i+k)%size] = numsCopy[i];
        }
    }
};