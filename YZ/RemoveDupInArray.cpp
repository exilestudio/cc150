//Given a sorted array, remove the duplicates in place such that each element appear only once and return the new length.

class Solution {
public:
    int removeDuplicates(vector<int>& nums) {
        vector<int> numsCopy;
        int size = nums.size();
    
        if (size < 1) return 0;
        
        unordered_set<int> hashSet;
        for (int i=0; i<size-1; i++) {
            if (hashSet.find(nums[i]) == hashSet.end()) {
                numsCopy.push_back(nums[i]);
            }
        }
    
        nums=numsCopy;
    }
};