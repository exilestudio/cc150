class Solution {
public:
    int countPrimes(int n) {
        int count = 0;
        
        for (int i = 0; i<n; i++){
            if (isPrimes(i)){
                count++;
            }
        }
        
        return count;
        
    }
    
    bool isPrimes (int n){
        if (n == 0) {return false;}
        else{
            int root = sqrt(n);
            for (int i=2; i <= root; i++){
                if (n % i == 0){
                    return false;
                }
            }
            
            return true;
        }
        
    }
};